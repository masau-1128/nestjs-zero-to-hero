import { Module } from '@nestjs/common';
import { TasksModule } from './tasks/tasks.module';
import {TypeOrmCoreModule} from "@nestjs/typeorm/dist/typeorm-core.module";
import {typeOrmConfig} from "./config/typeorm.config";
import { AuthModule } from './auth/auth.module';

@Module({
  imports: [
      TasksModule,
      TypeOrmCoreModule.forRoot(typeOrmConfig),
      AuthModule
  ],
})
export class AppModule {}
